# PyCryptodome
from Crypto.Signature import PKCS1_PSS
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
# Base64 for Serialization
from base64 import b64encode, b64decode

def sign(message, key):
	'''
	PyCrypto is kinda annoying because it does not
	allow us to import hashes from their digest
	to verify them against a signature. So instead
	what we are going to do is hash the coin, then
	save that hash in the coin, then hash that hash
	again and use that for the signature.
	'''
	# This is the hash we save
	h1 = SHA256.new()
	h1.update(message.encode())
	# This is the hash we sign
	h2 = SHA256.new()
	h2.update(h1.hexdigest().encode())
	signer = PKCS1_PSS.new(key)
	return {
		'signature': b64encode(signer.sign(h2)).decode(),
		'pubKey': key.publickey().export_key().decode(), 
		'hash': h1.hexdigest()
	}

def verify(message, signature, key):
	'''
	Because PyCrypto won't allow us to import
	a hash string as a native Hash object we 
	have to hash the stored hash again to verify
	against the signature. See sign() for more
	details.
	'''
	h = SHA256.new()
	h.update(message.encode())
	verifier = PKCS1_PSS.new(key)
	return verifier.verify(h, signature)
 
