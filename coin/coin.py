# PyCryptodome
from Crypto.Signature import PKCS1_PSS
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
# Coin Crypto
import crypto
# Base64 for Serialization
from base64 import b64encode, b64decode
# Exceptions
import exceptions
# General
import json


def mint(id):
	# Create new coin
	return {"id": id, "to": None, "history": []}

def send(coin, to, key=None):
	if key is None:
		key = RSA.importKey(open('rsa.priv').read())
	# Check if key has authority to send
	if coin['to'] and SHA256.new(key.publickey().export_key()).hexdigest() != coin['to']:
		raise exceptions.PermissionError('This is not your coin!')
	# Set the receipient
	coin['to'] = to
	# Sign the coin!
	coin['history'].append(crypto.sign(coin_to_json(coin), key))
	return coin

def coin_to_json(coin):
	# Remove whitespace and sort keys to ensure consistent
	# signing between different implementations
	return json.dumps(coin, separators=(',', ':'), sort_keys=True)