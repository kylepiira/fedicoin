﻿# Flask
from flask import (
	Flask, 
	Response,
	request
)
# Coin
import coin, validation
# PyCryptodome
from Crypto.PublicKey import RSA
# General
import json

# Init app
application = Flask(__name__)

# Views
@application.route('/mint')
def mint():
	c = coin.mint(1)
	for i in range(10):
		c = coin.send(c, '7d0391ed9f2ed42b75ed601b56a7c19f13813b35ffb8d5828982b460940fc786',
			key=RSA.importKey(open('rsa.priv').read()))
	return Response(json.dumps(c), 
					mimetype='application/json')

@application.route('/validate', methods=['POST'])
def validate():
	c = request.get_json(force=True)
	return Response(json.dumps(valiation.validate(c)), 
					mimetype='application/json')

if __name__ == "__main__":
	application.run()