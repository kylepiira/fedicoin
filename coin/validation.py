# PyCryptodome
from Crypto.Signature import PKCS1_PSS
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
# Coin Crypto
import crypto
import coin as utils
# Base64 for Serialization
from base64 import b64encode, b64decode

def validate(coin):
	checks = [
		# This verifies that the coin
		# has actually be signed at least
		# once by an issuer
		not_empty(coin),
		# This verifies that all of the
		# public keys and signatures are
		# valid, but does not verify
		# hashes
		keys_and_signatures(coin),
		# This verifies that all of the
		# hashes are correct by replaying
		# the transactions listed on the
		# coin, but does not verify
		# public keys and signatures
		replay(coin),
	]

	# All checks passed, we're a go houston!
	return all(checks)

def not_empty(coin):
	"""
	Checks that the coin has been
	issued.
	"""
	return len(coin['history']) > 0

def keys_and_signatures(coin):
	"""
	We verify that the hashes provided in the coin
	are signed by the provided public keys.
	This method assumes that the hashes are valid
	because those are checked elsewhere.
	"""
	for transaction in coin['history']:
		verified = crypto.verify(
			# We assume this is valid
			transaction['hash'],
			# Check if signature and pubKey match
			b64decode(transaction['signature'].encode()), 
			RSA.import_key(transaction['pubKey'])
		)

		# Check if the current signature / pubKey is valid
		if not verified:
			return False

	# If all of the signatures and public keys
	# were valid
	return True

def replay(coin):
	# We mint a new coin to replay with
	fake_coin = utils.mint(coin['id'])
	"""
	For each transaction in the coin we received
	we simulate the payment and rehash it. This
	simulation assumes that the pubKey and signature
	are valid since those are checked elsewhere.
	"""
	for i, transaction in enumerate(coin['history']):
		# Create receipent address from public key of next transaction
		try:
			fake_coin['to'] = SHA256.new(coin['history'][i + 1]['pubKey'].encode()).hexdigest()
		except IndexError:
			# If this is the latest transaction then
			# grab receipient from coin metadata
			fake_coin['to'] = coin['to']
		# Hash the coin and append transaction to history
		fake_coin['history'].append({
			# We assume the signature is valid
			'signature': transaction['signature'],
			# We assume the pubKey is valid
			'pubKey': transaction['pubKey'],
			# We recalculate the hash
			'hash': SHA256.new(utils.coin_to_json(fake_coin).encode()).hexdigest(),				
		})
		# Check if the recalculated hash matches the one in the coin
		# that we received.
		if fake_coin['history'][-1]['hash'] != transaction['hash']:
			return False

	# All of the hashes matched
	return True 
