# PyCryptodome
from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA
# Coin
import coin
import validation
import exceptions
# Testing
import unittest

class TestCoinSending(unittest.TestCase):
	def test_sending_fresh_coin(self):
		# Mint new coin
		c = coin.mint(1)

		# Private Key #1 (Issuer)
		issuer_priv_key = RSA.generate(2048)

		# Private Key #2 (Initial Receiver)
		receiver1_priv_key = RSA.generate(2048)

		# Private Key #3 (Second Receiver)
		receiver2_priv_key = RSA.generate(2048)

		# Generate Receiver addresses
		receiver1_address = SHA256.new(receiver1_priv_key.publickey().export_key()).hexdigest()
		receiver2_address = SHA256.new(receiver2_priv_key.publickey().export_key()).hexdigest()

		# Coin is sent from Issuer to Receiver #1
		c = coin.send(c, receiver1_address, key=issuer_priv_key)
		# Coin is sent from Receiver #1 to Reciever #2
		c = coin.send(c, receiver2_address, key=receiver1_priv_key)

		# Check that Receiver #2 owns the coin now
		self.assertEqual(c['to'], receiver2_address)
		# Check that coin is valid
		self.assertEqual(validation.validate(c), True)
		# Check that nobody else can send coin
		# Test issuer takeover
		issuer_takeover = True
		
		try:
			coin.send(c, receiver1_address, key=issuer_priv_key)
		except exceptions.PermissionError:
			issuer_takeover = False

		self.assertEqual(issuer_takeover, False)

		# Test receiver #1 takeover
		receiver1_takeover = True

		try:
			coin.send(c, receiver1_address, key=receiver1_priv_key)
		except exceptions.PermissionError:
			receiver1_takeover = False

		self.assertEqual(receiver1_takeover, False)

class TestCoinReceiving(unittest.TestCase):
	def test_coin_validation_with_blank(self):
		# Mint new coin
		c = coin.mint(1)

		self.assertEqual(validation.validate(c), False)

	def test_coin_validation_with_modification(self):
		# Mint new coin
		c = coin.mint(1)

		# Private Key #1 (Issuer)
		issuer_priv_key = RSA.generate(2048)

		# Private Key #2 (Initial Receiver)
		receiver1_priv_key = RSA.generate(2048)

		# Generate Receiver addresse
		receiver1_address = SHA256.new(receiver1_priv_key.publickey().export_key()).hexdigest()

		# Coin is sent from Issuer to Receiver #1
		c = coin.send(c, receiver1_address, key=issuer_priv_key)

		temp_coin = c
		temp_coin['id'] = 2
		self.assertEqual(validation.validate(temp_coin), False)

		temp_coin = c
		del(temp_coin['history'][0])
		self.assertEqual(validation.validate(temp_coin), False)

	def test_coin_validation_with_good(self):
		# Mint new coin
		c = coin.mint(1)

		# Private Key #1 (Issuer)
		issuer_priv_key = RSA.generate(2048)

		# Private Key #2 (Initial Receiver)
		receiver1_priv_key = RSA.generate(2048)

		# Generate Receiver addresse
		receiver1_address = SHA256.new(receiver1_priv_key.publickey().export_key()).hexdigest()

		# Coin is sent from Issuer to Receiver #1
		c = coin.send(c, receiver1_address, key=issuer_priv_key)

		self.assertEqual(validation.validate(c), True)

	def test_coin_signature_validation(self):
		# Mint new coin
		c = coin.mint(1)

		# Private Key #1 (Issuer)
		issuer_priv_key = RSA.generate(2048)

		# Private Key #2 (Initial Receiver)
		receiver1_priv_key = RSA.generate(2048)

		# Generate Receiver addresse
		receiver1_address = SHA256.new(receiver1_priv_key.publickey().export_key()).hexdigest()

		# Coin is sent from Issuer to Receiver #1
		c = coin.send(c, receiver1_address, key=issuer_priv_key)

		# Keys and signatures are valid
		self.assertEqual(validation.keys_and_signatures(c), True)

		# Remove the last character of the hash to break signatures
		c['history'][0]['hash'] = c['history'][0]['hash'][:-1]

		# Now the signatures should not be valid
		self.assertEqual(validation.keys_and_signatures(c), False)

	def test_coin_replay_validation(self):
		# Mint new coin
		c = coin.mint(1)

		# Private Key #1 (Issuer)
		issuer_priv_key = RSA.generate(2048)

		# Private Key #2 (Initial Receiver)
		receiver1_priv_key = RSA.generate(2048)

		# Private Key #3 (Second Receiver)
		receiver2_priv_key = RSA.generate(2048)

		# Generate Receiver addresses
		receiver1_address = SHA256.new(receiver1_priv_key.publickey().export_key()).hexdigest()
		receiver2_address = SHA256.new(receiver2_priv_key.publickey().export_key()).hexdigest()

		# Coin is sent from Issuer to Receiver #1
		c = coin.send(c, receiver1_address, key=issuer_priv_key)
		# Coin is sent from Receiver #1 to Reciever #2
		c = coin.send(c, receiver2_address, key=receiver1_priv_key)

		# Hashes are valid
		self.assertEqual(validation.replay(c), True)

		# Remove the last character of the first hash to break 2nd hash
		c['history'][0]['hash'] = c['history'][0]['hash'][:-1]

		# Hashes are not valid anymore
		self.assertEqual(validation.replay(c), False)

	def test_coin_empty_validation(self):
		# Mint new coin
		c = coin.mint(1)

		# This coin is empty
		self.assertEqual(validation.not_empty(c), False)

		# Private Key #1 (Issuer)
		issuer_priv_key = RSA.generate(2048)

		# Private Key #2 (Initial Receiver)
		receiver1_priv_key = RSA.generate(2048)

		# Generate Receiver addresse
		receiver1_address = SHA256.new(receiver1_priv_key.publickey().export_key()).hexdigest()

		# Coin is sent from Issuer to Receiver #1
		c = coin.send(c, receiver1_address, key=issuer_priv_key)

		# Now that the coin has been issued it not empty
		self.assertEqual(validation.not_empty(c), True)

if __name__ == '__main__':
	unittest.main()